﻿using System.Threading.Tasks;
using Xunit;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Microsoft.AspNetCore.Mvc;
using FluentAssertions;
using System;
using System.Collections.Generic;
using AutoFixture;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using FluentAssertions.Execution;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    
    public class SetPartnerPromoCodeLimitAsyncTests
    {

        private readonly Mock<IRepository<Partner>> mockedPartnerRepository;
        private readonly PartnersController partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {   
            mockedPartnerRepository = new Mock<IRepository<Partner>>();
            partnersController = new PartnersController(mockedPartnerRepository.Object);
        }

        public Partner CreatePartnerAutofixture()
        {
            var autoFixture = new Fixture();
            var partner = autoFixture.Build<Partner>().Without(o => o.PartnerLimits).Create();
            var partnerLimits = autoFixture.Build<PartnerPromoCodeLimit>().Without(o => o.Partner).Create();
            partnerLimits.Partner = partner;
            partnerLimits.CancelDate = DateTime.Parse("2021-07-07");
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>() { partnerLimits };

            return partner;
        }

        public SetPartnerPromoCodeLimitRequest CreateSetRequestAutofixture()
        {
            var autoFixture = new Fixture();
            var setRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();
            return setRequest;
        }


        //TODO: Add Unit Tests
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNull_NotFoundReturned()
        {
            //Arrange
            Guid partnerId = Guid.Parse("63729686-a368-4eeb-8bfa-cc69b6050d02");

            var partner = CreatePartnerAutofixture();
            var setRequest = CreateSetRequestAutofixture();
            partner.Id = partnerId;

            mockedPartnerRepository.Setup(o => o.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), setRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
            
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsInactive_BadRequestObjectReturned()
        {
            //Arrange
            var partner = CreatePartnerAutofixture();
            var setRequest = CreateSetRequestAutofixture();
            partner.IsActive = false;

            mockedPartnerRepository.Setup(o => o.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setRequest);

            //Assert

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerLimitIsSet_NumberIssuedPromoCodesIsZero()
        {
            //Arrange
            var partner = CreatePartnerAutofixture();
            var setRequest = CreateSetRequestAutofixture();

            mockedPartnerRepository.Setup(o => o.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            mockedPartnerRepository.Setup(o => o.UpdateAsync(partner));

            //Act

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setRequest);

            //Assert
            using (new AssertionScope())
            {
                partner.NumberIssuedPromoCodes.Should().Be(0);
                //Не понял пункт про "Если лимит закончился, то не обнуляется"
            }
             // Не работает
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerLimitIsSet_CancelDateIsSet()
        {
            //Arrange
            var partner = CreatePartnerAutofixture();
            var setRequest = CreateSetRequestAutofixture();
            var partnerLimits = partner.PartnerLimits.First();

            mockedPartnerRepository.Setup(o => o.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            mockedPartnerRepository.Setup(o => o.UpdateAsync(partner));

            //Act

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setRequest);

            //Assert
            using (new AssertionScope())
            {
                partnerLimits.CancelDate.Should().NotHaveYear(2021);
            }
        }

        [Fact]
       public async Task SetPartnerPromoCodeLimitAsync_LimitLessThanZero_BadRequestObjectReturned()
        {
            //Arrange
            var partner = CreatePartnerAutofixture();
            var setRequest = CreateSetRequestAutofixture();

            mockedPartnerRepository.Setup(o => o.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            mockedPartnerRepository.Setup(o => o.UpdateAsync(partner));

            setRequest.Limit = -150;

            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(o => o.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setRequest);

            //Assert

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitCreated_CreatedAtActionReturned()
        {
            //Arrange
            var partner = CreatePartnerAutofixture();
            var setRequest = CreateSetRequestAutofixture();

            mockedPartnerRepository.Setup(o => o.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            mockedPartnerRepository.Setup(o => o.UpdateAsync(partner));

            //Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setRequest);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }

    }
}